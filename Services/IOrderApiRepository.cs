﻿using System;
using TechnicalAssigmentAlbelli.Classes;
using TechnicalAssigmentAlbelli.Models;

namespace TechnicalAssigmentAlbelli.Services
{
    public interface IOrderApiRepository
    {
        void AddOrder(Order order);
        void DeleteOrder(Order order);
        OrderDto GetOrder(Guid orderId);
        bool Save();
    }
}
