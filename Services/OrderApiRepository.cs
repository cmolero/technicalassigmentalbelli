﻿using System;
using System.Collections.Generic;
using System.Linq;
using TechnicalAssigmentAlbelli.Classes;
using TechnicalAssigmentAlbelli.DbContexts;
using TechnicalAssigmentAlbelli.Helpers;
using TechnicalAssigmentAlbelli.Models;

namespace TechnicalAssigmentAlbelli.Services
{
    public class OrderApiRepository : IOrderApiRepository
    {
        private readonly OrderApiContext _context;
        public OrderApiRepository(OrderApiContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }
        public void AddOrder(Order order)
        {
            if (order == null)
            {
                throw new ArgumentNullException(nameof(order));
            }
            order.Products.ToList().ForEach(s => s.OrderId = order.OrderId);
            _context.Orders.Add(order);
            _context.OrderProduct.AddRange(order.Products);
        }
        public void DeleteOrder(Order order)
        {
            _context.Orders.Remove(order);
        }
        public OrderDto GetOrder(Guid orderId)
        {
            if (orderId == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(orderId));
            }
            bool orderExist = _context.Orders.Where(o => o.OrderId == orderId).Any();
            if (!orderExist)
            {
                return null;
            }
            var pList = _context.OrderProduct.Where(o => o.OrderId == orderId).ToList();
            double minWidth = MinWidthOffsetExtension.GetMinWidth(GetProductInfo(orderId));
            return new OrderDto()
            {
                OrderId = orderId,
                Products = pList,
                MinWidth = minWidth
            };
        }
        public bool Save()
        {
            return _context.SaveChanges() >= 0;
        }
        public IQueryable<OrderProductWidth> GetProductInfo(Guid orderId)
        {
            var pList = _context.OrderProduct.
                Join(_context.Products, p => p.ProductType,
                    op => op.ProductType,
                    (p, op) => new OrderProductWidth
                    {
                        OrderId = p.OrderId,
                        ProductType = op.ProductType,
                        Quantity = p.Quantity,
                        RequiredBinWidth = op.RequiredBinWidth
                    }).Where(w => w.OrderId == orderId);
            return pList;
        }
    }
}
