﻿using System;
using System.Collections.Generic;
using TechnicalAssigmentAlbelli.Classes;

namespace TechnicalAssigmentAlbelli.Models
{
    public class OrderDto
    {
        public Guid OrderId { get; set; }
        public List<OrderProduct> Products { get; set; }
        public double MinWidth { get; set; }
    }
}
