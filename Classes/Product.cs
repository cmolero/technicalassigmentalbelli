﻿using System.ComponentModel.DataAnnotations;

namespace TechnicalAssigmentAlbelli.Classes
{
    public class Product
    {
        [Key]
        public int ProductId { get; set; }
        [Required]
        [MaxLength(100)]
        public string ProductType { get; set; }
        [Required]
        public double RequiredBinWidth { get; set; }
    }
}
