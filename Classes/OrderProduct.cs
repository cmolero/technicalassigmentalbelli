﻿using Microsoft.AspNetCore.Components.Forms;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;

namespace TechnicalAssigmentAlbelli.Classes
{
    [NotMapped]
    public class OrderProduct
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public Guid OrderId { get; set; }
        [Required]
        public string ProductType { get; set; }
        [Range(1,9999)]
        [Required]
        public int Quantity { get; set; }
    }
    [NotMapped]
    public class OrderProductWidth
    {
        public Guid OrderId { get; set; }
        public string ProductType { get; set; }
        public int Quantity { get; set; }
        public double RequiredBinWidth { get; set; }
    }
}
