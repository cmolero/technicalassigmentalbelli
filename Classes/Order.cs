﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TechnicalAssigmentAlbelli.Classes
{
    public class Order
    {
        Guid _orderId;
        [Key]
        public Guid OrderId { get { return _orderId; } set { _orderId = value; } }
        [Required]
        [NotMapped]
        public List<OrderProduct> Products { get; set; }
    }
}
