﻿using Microsoft.EntityFrameworkCore;
using TechnicalAssigmentAlbelli.Classes;

namespace TechnicalAssigmentAlbelli.DbContexts
{
    public class OrderApiContext : DbContext
    {
        public OrderApiContext(DbContextOptions<OrderApiContext> options) : base(options)
        {

        }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderProduct> OrderProduct {get;set;}
        public DbSet<Product> Products { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //seeds database with data, only for technical assignment purposes
            modelBuilder.Entity<Product>().HasKey(p => p.ProductId);
            modelBuilder.Entity<Order>().HasKey(o => o.OrderId);
            modelBuilder.Entity<Product>().HasData(
                new Product()
                {
                    ProductId = 1,
                    ProductType = "photoBook",
                    RequiredBinWidth = 19
                },
                new Product()
                {
                    ProductId = 2,
                    ProductType = "calendar",
                    RequiredBinWidth = 10
                },
                new Product()
                {
                    ProductId = 3,
                    ProductType = "photoBook",
                    RequiredBinWidth = 19
                },
                new Product()
                {
                    ProductId = 4,
                    ProductType = "canvas",
                    RequiredBinWidth = 16
                },
                new Product()
                {
                    ProductId = 5,
                    ProductType = "cards",
                    RequiredBinWidth = 4.7
                },
                new Product()
                {
                    ProductId = 6,
                    ProductType = "mug",
                    RequiredBinWidth = 94
                });
            modelBuilder.Entity<OrderProduct>();
            base.OnModelCreating(modelBuilder);
        }
    }
}
