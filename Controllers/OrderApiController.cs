﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using TechnicalAssigmentAlbelli.Classes;
using TechnicalAssigmentAlbelli.Models;
using TechnicalAssigmentAlbelli.Services;

namespace TechnicalAssigmentAlbelli.Controllers
{
    [ApiController]
    [Route("api/order")]
    public class OrderApiController : ControllerBase
    {
        private readonly IOrderApiRepository _orderApiRepository;
        private readonly IMapper _mapper;
        public OrderApiController(IOrderApiRepository orderApiRepository, IMapper mapper)
        {
            _orderApiRepository = orderApiRepository ?? throw new ArgumentNullException(nameof(orderApiRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        [HttpGet("{orderId}", Name = "GetOrder")]
        public ActionResult<IEnumerable<OrderDto>> GetOrder(Guid orderId)
        {
            var orderInfo = _orderApiRepository.GetOrder(orderId);            
            if (orderInfo == null)
            {
                return NotFound();
            }
            return Ok(_mapper.Map<OrderDto>(orderInfo));
        }
        [HttpPost]
        public ActionResult<OrderDto> CreateOrder(Order order)
        {
            //var orderEntity = _mapper.Map<Order>(order);
            order.OrderId = Guid.NewGuid();
            _orderApiRepository.AddOrder(order);
            _orderApiRepository.Save();
            var newOrder = _mapper.Map<OrderDto>(order);
            return CreatedAtRoute("GetOrder", new { orderId = newOrder.OrderId }, newOrder);
        }

    }
}
