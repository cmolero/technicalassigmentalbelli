﻿using System;
using System.Linq;
using TechnicalAssigmentAlbelli.Classes;

namespace TechnicalAssigmentAlbelli.Helpers
{
    public static class MinWidthOffsetExtension
    {
        public static double GetMinWidth(IQueryable<OrderProductWidth> pList)
        {
            int nMug = pList.Where(pType => pType.ProductType == "mug").Select(x => x.Quantity).FirstOrDefault();
            int mugWidth = (int)Math.Ceiling(nMug / 4.0);
            double minWidth = 0;
            foreach(var item in pList)
            {
                if (item.ProductType != "mug")
                {
                    minWidth = minWidth + (item.Quantity * item.RequiredBinWidth);
                }
                else
                {
                    minWidth = minWidth + (mugWidth * item.RequiredBinWidth);
                }
            }

            return minWidth;
        }
    }
}
