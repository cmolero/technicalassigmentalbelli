﻿using AutoMapper;
using TechnicalAssigmentAlbelli.Classes;
using TechnicalAssigmentAlbelli.Models;

namespace TechnicalAssigmentAlbelli.Profiles
{
    public class OrderProfile:Profile
    {
        public OrderProfile()
        {
            CreateMap<Order, OrderDto>().ForMember(x => x.MinWidth, a => a.Ignore());
        }
    }
}
